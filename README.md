# HealthPoint api

## Getting started

### Installation

```bash
$ yarn
```

### Running the app

```bash
# development
$ TZ=UTC NODE_ENV=development yarn start

# watch mode
$ TZ=UTC NODE_ENV=development yarn start:dev

# watch and debug mode
$ TZ=UTC NODE_ENV=development yarn start:debug
```

### Running the scheduler

```bash
$ TZ=UTC NODE_ENV=development yarn start:scheduler
```

### Running the workers

```bash
$ TZ=UTC NODE_ENV=development yarn start:worker
```

### Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```
