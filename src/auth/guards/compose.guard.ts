import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { AllowGuard } from './allow.guard';
import { AuthGuard } from './auth.guard';
import { RoleGuard } from './role.guard';

@Injectable()
export class ComposeGuard implements CanActivate {
  constructor(
    private readonly allowGuard: AllowGuard,
    private readonly authGuard: AuthGuard,
    private readonly roleGuard: RoleGuard,
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    return (
      (await this.allowGuard.canActivate(context)) ||
      ((await this.authGuard.canActivate(context)) && (await this.roleGuard.canActivate(context)))
    );
  }
}
