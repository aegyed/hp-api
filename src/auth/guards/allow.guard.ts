import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ConfigService } from '../../core/config/config.service';
import { AllowEnum } from '../decorators/allow.decorator';

@Injectable()
export class AllowGuard implements CanActivate {
  constructor(private reflector: Reflector, private config: ConfigService) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const endpointAllow =
      this.reflector.get<string[]>('allow', context.getHandler()) ||
      this.reflector.get<string[]>('allow', context.getClass());

    if (!endpointAllow || endpointAllow.length === 0) {
      return false;
    }

    // skip for public
    if (endpointAllow.includes(AllowEnum.PUBLIC)) {
      return true;
    }

    // skip for known hosts
    const request = context.switchToHttp().getRequest();
    if (endpointAllow.includes(AllowEnum.WHITELIST)) {
      const whitelist: string[] = JSON.parse(this.config.get<string>('ALLOW_WHITELIST'));
      if (whitelist.includes(request.connection.remoteAddress)) {
        return true;
      }
    }
  }
}
