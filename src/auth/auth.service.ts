import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

import { MailService } from '../core/mailer/mail.service';
import { UserEntity } from '../user/user.entity';
import { UserRepository } from '../user/user.repository';
import { ChangePasswordDto, ForgotPasswordDto, LoginDto, ResetPasswordDto, SignupDto } from './dto';

@Injectable()
export class AuthService {

  constructor(
    private readonly jwtService: JwtService,
    private readonly mailService: MailService,
    private readonly userRepository: UserRepository,
  ) { }

  async createAuthToken(user: UserEntity): Promise<string> {
    return this.jwtService.signAsync({ id: user.id });
  }

  async generatePasswordHash(password: string): Promise<string> {
    return bcrypt.hash(password, 10);
  }

  async validatePasswordHash(password: string, hash: string): Promise<boolean> {
    return bcrypt.compare(password, hash || '');
  }

  async login(loginDto: LoginDto): Promise<{ user: UserEntity; token: string }> {
    const user = await this.userRepository.findOne({ email: loginDto.email });
    if (!user) {
      throw new UnauthorizedException();
    }
    const isPasswordValid = await this.validatePasswordHash(
      loginDto.password,
      user.password,
    );
    if (!isPasswordValid) {
      throw new UnauthorizedException();
    }
    const token = await this.createAuthToken(user);
    await this.userRepository.update(user.id, { lastLogin: new Date() });
    return { user, token }
  }

  async signup(signupDto: SignupDto): Promise<{ user: UserEntity; token: string }> {
    const passwordHash = await this.generatePasswordHash(signupDto.password);
    const user = await this.userRepository.save({ ...signupDto, password: passwordHash });
    const token = await this.createAuthToken(user);
    await this.mailService.sendWelcomeEmail(user);
    return { user, token }
  }

  async changePassword(user: UserEntity, changePasswordDto: ChangePasswordDto): Promise<void> {
    const passwordHash = await this.generatePasswordHash(changePasswordDto.password);
    await this.userRepository.update(user.id, { password: passwordHash });
    await this.mailService.sendPasswordChangedEmail(user);
    return void (0);
  }

  async forgotPassword(forgotPasswordDto: ForgotPasswordDto): Promise<void> {
    const user = await this.userRepository.findOneOrFail({
      email: forgotPasswordDto.email,
    });
    const secret = user.password;
    const token = jwt.sign({ id: user.id }, secret, {
      expiresIn: 24 * 60 * 60 * 1000, // 1 day
    });
    await this.mailService.sendForgotPasswordEmail(user, token);
    return void (0);
  }

  async resetPassword(resetPasswordDto: ResetPasswordDto): Promise<void> {
    const user = await this.userRepository.findOneOrFail({
      email: resetPasswordDto.email,
    });
    const secret = user.password;
    let decoded: any;
    try {
      decoded = jwt.verify(resetPasswordDto.token, secret, {
        ignoreExpiration: false,
      });
    } catch (err) {
      throw new UnauthorizedException();
    }
    if (decoded.id !== user.id) {
      throw new UnauthorizedException();
    }
    await this.userRepository.update(user.id, {
      password: await this.generatePasswordHash(resetPasswordDto.password),
    });
    await this.mailService.sendPasswordChangedEmail(user);
    return void (0);
  }

}
