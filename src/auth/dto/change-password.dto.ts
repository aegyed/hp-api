import { IsNotEmpty, IsString, MinLength } from 'class-validator';

export class ChangePasswordDto {

  @IsString()
  @MinLength(6)
  @IsNotEmpty()
  readonly password: string;

}
