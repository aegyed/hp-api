import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MinLength,
  Validate,
  IsAlpha,
  IsPositive,
  IsISO8601,
} from 'class-validator';
import { MeasurementSystem } from '../../user/user.entity';
import { IsUniqueEmail, BirthDateValidator } from '../../user/validators';

export class SignupDto {

  @IsString()
  @IsEmail()
  @IsNotEmpty()
  @Validate(IsUniqueEmail)
  readonly email: string;

  @IsString()
  @MinLength(6)
  @IsNotEmpty()
  readonly password: string;

  @IsString()
  @IsAlpha()
  @IsNotEmpty()
  readonly firstName: string;

  @IsString()
  @IsAlpha()
  @IsOptional()
  readonly lastName?: string;

  @IsISO8601()
  @Validate(BirthDateValidator)
  @IsNotEmpty()
  readonly birthDate?: Date;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  readonly height?: number;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  readonly weight?: number;

  @IsEnum(MeasurementSystem)
  @IsOptional()
  readonly measurementSystem?: MeasurementSystem;

}
