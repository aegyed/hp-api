import { forwardRef, Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { ConfigService } from '../core/config/config.service';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { AllowGuard } from './guards/allow.guard';
import { AuthGuard } from './guards/auth.guard';
import { ComposeGuard } from './guards/compose.guard';
import { RoleGuard } from './guards/role.guard';
import { JwtStrategy } from './passport/jwt.strategy';

@Module({
  imports: [
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET_KEY'),
      }),
    }),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    forwardRef(() => UserModule),
  ],
  controllers: [AuthController],
  providers: [
    JwtStrategy,
    AllowGuard,
    AuthGuard,
    RoleGuard,
    AuthService,
    {
      provide: APP_GUARD,
      useClass: ComposeGuard,
    },
  ],
  exports: [AuthService],
})
export class AuthModule { }
