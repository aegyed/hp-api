import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';

import { UserDto } from '../user/dto';
import { UserEntity } from '../user/user.entity';
import { AuthService } from './auth.service';
import { Allow, AllowEnum, CurrentUser } from './decorators'
import { ChangePasswordDto, ForgotPasswordDto, LoginDto, ResetPasswordDto, SignupDto } from './dto';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Allow(AllowEnum.PUBLIC)
  @Post('login')
  @HttpCode(HttpStatus.OK)
  async login(@Body() loginDto: LoginDto): Promise<{ user: UserDto; token: string }> {
    const { user, token } = await this.authService.login(loginDto);
    return { user: new UserDto(user), token };
  }

  @Allow(AllowEnum.PUBLIC)
  @Post('signup')
  @HttpCode(HttpStatus.CREATED)
  async signup(@Body() signupDto: SignupDto): Promise<{ user: UserDto; token: string }> {
    const { user, token } = await this.authService.signup(signupDto);
    return { user: new UserDto(user), token };
  }

  @Post('change-password')
  @HttpCode(HttpStatus.NO_CONTENT)
  async changePassword(@CurrentUser() user: UserEntity, @Body() changePasswordDto: ChangePasswordDto): Promise<void> {
    await this.authService.changePassword(user, changePasswordDto)
    return void (0);
  }

  @Allow(AllowEnum.PUBLIC)
  @Post('forgot-password')
  @HttpCode(HttpStatus.NO_CONTENT)
  async forgotPassword(@Body() forgotPasswordDto: ForgotPasswordDto): Promise<void> {
    await this.authService.forgotPassword(forgotPasswordDto);
    return void (0);
  }

  @Allow(AllowEnum.PUBLIC)
  @Post('reset-password')
  @HttpCode(HttpStatus.NO_CONTENT)
  async resetPassword(@Body() resetPasswordDto: ResetPasswordDto): Promise<void> {
    await this.authService.resetPassword(resetPasswordDto);
    return void (0);
  }

}
