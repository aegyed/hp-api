import { CustomDecorator, SetMetadata } from '@nestjs/common';

export const Allow = (...roles: AllowEnum[]): CustomDecorator<string> => SetMetadata('allow', roles);

export enum AllowEnum {
  PUBLIC = 'public',
  WHITELIST = 'whitelist',
}
