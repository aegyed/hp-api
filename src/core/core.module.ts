import { DynamicModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { ConfigModule } from './config/config.module';
import { RequestContextMiddleware } from './context';
import { DbModule } from './db/db.module';
import { MailerModule } from './mailer/mailer.module';
import { QueueModule } from './queue/queue.module';

type AppType = 'app' | 'scheduler' | 'worker';

@Module({
  imports: [
    ConfigModule,
    DbModule,
    QueueModule,
  ],
  exports: [QueueModule],
})
export class CoreModule implements NestModule {
  static type: AppType;

  static for(type: AppType): DynamicModule {
    CoreModule.type = type;

    const imports = [];
    switch (type) {
      case 'app':
        imports.push(MailerModule)
        break
      case 'scheduler':
        imports.push(ScheduleModule.forRoot())
        break
      case 'worker':
        imports.push(MailerModule)
        break
    }

    return {
      module: CoreModule,
      imports,
    }
  }

  configure(consumer: MiddlewareConsumer): void {
    if (CoreModule.type === 'app') {
      consumer.apply(RequestContextMiddleware).forRoutes('*');
    }
  }
}