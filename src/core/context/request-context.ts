import cls from 'cls-hooked';
import { Request, Response } from 'express'
import { UserEntity } from '../../user/user.entity';


export class RequestContext {
  readonly id: number;
  readonly request: Request;
  readonly response: Response;

  constructor(request: Request, response: Response) {
    this.id = Math.random();
    this.request = request;
    this.response = response;
  }

  static currentRequestContext(): RequestContext {
    const session = cls.getNamespace(RequestContext.name);
    if (session && session.active) {
      return session.get(RequestContext.name);
    }

    return null;
  }

  static currentRequest(): Request {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      return requestContext.request;
    }

    return null;
  }

  static currentUser(): UserEntity {
    const requestContext = RequestContext.currentRequestContext();

    if (requestContext) {
      // tslint:disable-next-line
      const user = requestContext.request['user'] as UserEntity;

      if (user) {
        return user;
      }
    }

    return null;
  }

}
