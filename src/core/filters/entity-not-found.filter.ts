import { ArgumentsHost, Catch, ExceptionFilter, Logger, NotFoundException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { EntityNotFoundError } from 'typeorm/error/EntityNotFoundError';

@Catch(EntityNotFoundError)
export class EntityNotFoundFilter implements ExceptionFilter {
  private logger = new Logger(EntityNotFoundFilter.name);

  constructor(public reflector: Reflector) { }

  catch(exception: EntityNotFoundError, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse();

    this.logger.error(exception, exception.stack);

    const err = new NotFoundException();
    return res.status(err.getStatus()).json(err.getResponse());
  }
}
