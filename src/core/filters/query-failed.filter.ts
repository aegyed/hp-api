import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { QueryFailedError } from 'typeorm';

@Catch(QueryFailedError)
export class QueryFailedFilter implements ExceptionFilter {
  private logger = new Logger(QueryFailedError.name);

  constructor(public reflector: Reflector) { }

  catch(exception: QueryFailedError, host: ArgumentsHost): any {
    const ctx = host.switchToHttp();
    const res = ctx.getResponse();

    this.logger.error(exception, exception.stack);

    const err = new InternalServerErrorException();
    return res.status(err.getStatus()).json(err.getResponse());
  }
}
