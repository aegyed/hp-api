import { Global, Logger, Module } from '@nestjs/common';
import { ConfigModule as NestConfigModule } from '@nestjs/config';
import { ConfigService } from './config.service';

const validEnvironments = ['production', 'development', 'test']
const env = process.env.NODE_ENV;
if (!validEnvironments.includes(env)) {
  throw new Error(`Invalid NODE_ENV: ${env}`);
} else {
  Logger.log(`Running in "${env}" environment.`, 'ConfigModule');
}

if (process.env.TZ !== 'UTC') {
  throw new Error(`Invalid timezone: ${process.env.TZ}`);
}

@Global()
@Module({
  imports: [
    NestConfigModule.forRoot({
      envFilePath: `.${env}.env`,
      expandVariables: true,
    }),
  ],
  providers: [ConfigService],
  exports: [ConfigService],
})
export class ConfigModule { }
