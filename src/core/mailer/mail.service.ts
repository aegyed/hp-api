import { MailerService } from '@nest-modules/mailer';
import { Injectable } from '@nestjs/common';

import { UserEntity } from '../../user/user.entity';

@Injectable()
export class MailService {
  constructor(private readonly mailerService: MailerService) {}

  async sendWelcomeEmail(userEntity: UserEntity): Promise<void> {
    await this.mailerService.sendMail({
      to: userEntity.email,
      subject: 'Welcome to HealthPoint',
      template: 'welcome',
      context: {
        firstName: userEntity.firstName,
      },
    })
  }

  async sendPasswordChangedEmail(userEntity: UserEntity): Promise<void> {
    await this.mailerService.sendMail({
      to: userEntity.email,
      subject: 'Your password has been changed',
      template: 'password-changed',
      context: {
        firstName: userEntity.firstName,
      },
    })
  }

  async sendForgotPasswordEmail(userEntity: UserEntity, token: string): Promise<void> {
    await this.mailerService.sendMail({
      to: userEntity.email,
      subject: 'Reset password instructions',
      template: 'forgot-password',
      context: {
        firstName: userEntity.firstName,
        email: userEntity.email,
        token,
      },
    })
  }
}