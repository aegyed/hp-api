import {
  HandlebarsAdapter,
  MailerOptions as NestMailerOptions,
  MailerOptionsFactory,
} from '@nest-modules/mailer';
import { Injectable } from '@nestjs/common';

import { ConfigService } from '../config/config.service';

@Injectable()
export class MailerOptions implements MailerOptionsFactory {
  constructor(private readonly configService: ConfigService) { }

  createMailerOptions(): NestMailerOptions {
    return {
      transport: {
        host: this.configService.get<string>('MAILER_HOST'),
        port: this.configService.get<number>('MAILER_PORT'),
        auth: {
          user: this.configService.get<string>('MAILER_USERNAME'),
          pass: this.configService.get<string>('MAILER_PASSWORD'),
        },
      },
      defaults: {
        from: `HealthPoint <${this.configService.get<string>('SUPPORT_EMAIL')}>`,
      },
      template: {
        dir: `${process.cwd()}/src/core/mailer/templates`,
        adapter: new HandlebarsAdapter(),
        options: {
          strict: true,
        },
      },
    }
  }

}