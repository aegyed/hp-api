import { MailerModule as NestMailerModule } from '@nest-modules/mailer';
import { Global, Module } from '@nestjs/common';

import { MailService } from './mail.service';
import { MailerOptions } from './mailer-options';

@Global()
@Module({
  imports: [
    NestMailerModule.forRootAsync({
      useClass: MailerOptions,
    }),
  ],
  providers: [MailService],
  exports: [MailService],
})
export class MailerModule { }