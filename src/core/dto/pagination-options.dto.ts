import { Transform } from 'class-transformer';
import { IsOptional, Max, Min } from 'class-validator';

import { IPaginationOptions } from '../pagination';

export abstract class PaginationOptionsDto implements IPaginationOptions {

  @IsOptional()
  @Min(1)
  @Transform(({ value }) => parseInt(value, 10))
  readonly page = 1;

  @IsOptional()
  @Min(10)
  @Max(50)
  @Transform(({ value }) => parseInt(value, 10))
  readonly limit = 10;

}
