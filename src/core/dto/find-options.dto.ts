import { PaginationOptionsDto } from './pagination-options.dto';

export enum OrderType {
  DESC = 'DESC',
  ASC = 'ASC',
}

export abstract class FindOptionsDto<T> extends PaginationOptionsDto {
  // TODO ...
  // [P in keyof T]?: any;

  abstract readonly order?: { [P in keyof T]?: OrderType };
}