/* eslint-disable @typescript-eslint/no-use-before-define */
import querystring from 'querystring';
import {
  FindConditions,
  FindManyOptions,
  Repository,
  SelectQueryBuilder,
} from 'typeorm';

import { RequestContext } from '../context';
import { Pagination } from './pagination';
import { IPaginationOptions } from './pagination-options';

export async function paginate<T>(
  repository: Repository<T>,
  options: IPaginationOptions,
  findOptions?: FindConditions<T> | FindManyOptions<T>,
): Promise<Pagination<T>>;

export async function paginate<T>(
  queryBuilder: SelectQueryBuilder<T>,
  options: IPaginationOptions,
): Promise<Pagination<T>>;

export async function paginate<T>(
  repositoryOrQueryBuilder: Repository<T> | SelectQueryBuilder<T>,
  options: IPaginationOptions,
  findOptions?: FindConditions<T> | FindManyOptions<T>,
): Promise<Pagination<T>> {
  return repositoryOrQueryBuilder instanceof Repository
    ? paginateRepo<T>(repositoryOrQueryBuilder, options, findOptions)
    : paginateQueryBuilder(repositoryOrQueryBuilder, options);
}

async function paginateRepo<T>(
  repository: Repository<T>,
  options: IPaginationOptions,
  findOptions?: FindConditions<T> | FindManyOptions<T>,
): Promise<Pagination<T>> {
  const { page, limit } = saniziteOptions(options);

  const [items, total] = await repository.findAndCount({
    skip: (page - 1) * limit,
    take: limit,
    ...findOptions,
  });

  return createPagination<T>(items, total, page, limit);
}

async function paginateQueryBuilder<T>(
  queryBuilder: SelectQueryBuilder<T>,
  options: IPaginationOptions,
): Promise<Pagination<T>> {
  const { page, limit } = saniziteOptions(options);

  const [items, total] = await queryBuilder
    .skip((page - 1) * limit)
    .take(limit)
    .getManyAndCount();

  return createPagination<T>(items, total, page, limit);
}

function saniziteOptions({ page, limit }: IPaginationOptions): IPaginationOptions {
  return {
    page: page < 1 ? 1 : page,
    limit: limit < 0 ? 10 : limit,
  }
}

function createPagination<T>(
  items: T[],
  total: number,
  page: number,
  limit: number,
) {
  const hasNext = total / limit > page;
  const hasPrev = page > 1;
  const routes = {
    next: hasNext ? createRoute(page + 1) : null,
    prev: hasPrev ? createRoute(page - 1) : null,
  };
  const totalPages = Math.ceil(total / limit);

  return new Pagination(
    items,
    items.length,
    total,
    totalPages,
    routes.next,
    routes.prev,
  );
}

function createRoute(page: number) {
  const { path, query } = RequestContext.currentRequest();
  const qs = querystring.stringify({ ...query, page })
  return `${path}/?${qs}`
}
