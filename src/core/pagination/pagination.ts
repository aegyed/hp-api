export class Pagination<T> {
  constructor(
    public readonly items: T[],
    public readonly itemCount: number,
    public readonly totalItems: number,
    public readonly totalPages: number,
    public readonly next?: string,
    public readonly prev?: string,
  ) { }
}
