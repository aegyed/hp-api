import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { HpDbOptions } from './hp-db-options';

export enum DbConnection {
  HP_RW = 'default',
}

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      name: DbConnection.HP_RW,
      useClass: HpDbOptions,
    }),
  ],
})
export class DbModule { }
