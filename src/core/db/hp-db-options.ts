import { Injectable } from '@nestjs/common';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { ConfigService } from '../config/config.service';

@Injectable()
export class HpDbOptions implements TypeOrmOptionsFactory {
  constructor(private readonly configService: ConfigService) { }

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: 'mysql',
      host: this.configService.get<string>('MYSQL_HP_HOST'),
      port: this.configService.get<number>('MYSQL_HP_PORT'),
      username: this.configService.get<string>('MYSQL_HP_USERNAME'),
      password: this.configService.get<string>('MYSQL_HP_PASSWORD'),
      database: this.configService.get<string>('MYSQL_HP_DATABASE'),
      entities: [
        __dirname + '/../../user/**/*.entity{.ts,.js}',
        __dirname + '/../../fast/**/*.entity{.ts,.js}',
        __dirname + '/../../quote/**/*.entity{.ts,.js}',
      ],
      logging: this.configService.isDevelopment,
      synchronize: this.configService.isDevelopment,
      // migrations: [
      //   __dirname + '/../../../migrations/**/*{.ts,.js}',
      // ],
      // migrationsRun: false,
      // cli: {
      //   migrationsDir: 'migrations',
      // },

    }
  }

}
