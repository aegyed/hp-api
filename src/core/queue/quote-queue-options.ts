import { BullModuleOptions, BullOptionsFactory } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';

import { ConfigService } from '../config/config.service';

@Injectable()
export class QuoteQueueOptions implements BullOptionsFactory {
  constructor(private readonly configService: ConfigService) { }

  createBullOptions(): BullModuleOptions {
    return {
      prefix: 'quote',
      defaultJobOptions: {
        removeOnComplete: true,
        attempts: 3,
        timeout: 60 * 1000,
        backoff: {
          type: 'exponential',
          delay: 60 * 1000,
        },
      },
      redis: {
        host: this.configService.get<string>('REDIS_HOST'),
        port: this.configService.get<number>('REDIS_PORT'),
        db: 1,
      },
    }
  }

}
