import { BullModule } from '@nestjs/bull';
import { Module, Global } from '@nestjs/common';

import { QuoteQueueOptions } from './quote-queue-options';
import { FastQueueOptions } from './fast-queue-options';

export enum QueueName {
  Quote = 'quote',
  Fast = 'fast',
}

@Global()
@Module({
  imports: [
    BullModule.registerQueueAsync({
      name: QueueName.Quote,
      useClass: QuoteQueueOptions,
    }),
    BullModule.registerQueueAsync({
      name: QueueName.Fast,
      useClass: FastQueueOptions,
    }),
  ],
  exports: [BullModule],
})
export class QueueModule { }
