import { BullModuleOptions, BullOptionsFactory } from '@nestjs/bull';
import { Injectable } from '@nestjs/common';

import { ConfigService } from '../config/config.service';

@Injectable()
export class FastQueueOptions implements BullOptionsFactory {
  constructor(private readonly configService: ConfigService) { }

  createBullOptions(): BullModuleOptions {
    return {
      prefix: 'fast',
      defaultJobOptions: {
        removeOnComplete: true,
        attempts: 3,
        timeout: 60 * 1000,
        backoff: {
          type: 'exponential',
          delay: 60 * 1000,
        },
      },
      // TODO: use `createClient` for re-using redis connections
      redis: {
        host: this.configService.get<string>('REDIS_HOST'),
        port: this.configService.get<number>('REDIS_PORT'),
        db: 1,
      },
    }
  }

}
