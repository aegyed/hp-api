import { Module } from '@nestjs/common';

import { CoreModule } from './core/core.module';
import { SchedulerService } from './scheduler.service';

@Module({
  imports: [CoreModule.for('scheduler')],
  providers: [SchedulerService],
})
export class SchedulerModule { }