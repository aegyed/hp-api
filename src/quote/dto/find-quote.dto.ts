import { IsDefined, IsISO8601 } from 'class-validator';

// https://github.com/typestack/class-validator/issues/115
export class FindQuoteDto {

  @IsISO8601()
  @IsDefined()
  date: string;

}