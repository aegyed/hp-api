import { QuoteEntity } from '../quote.entity';

export class QuoteDto {
  readonly date: string;
  readonly author: string;
  readonly quote: string;

  constructor(quoteEntity: QuoteEntity) {
    this.date = quoteEntity.date
    this.author = quoteEntity.author
    this.quote = quoteEntity.quote
  }
}
