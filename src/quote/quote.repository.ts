import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { QuoteEntity } from './quote.entity';

@EntityRepository(QuoteEntity)
export class QuoteRepository extends Repository<QuoteEntity> {}
