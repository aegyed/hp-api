import { HttpModule, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { QuoteClient } from './quote.client';
import { QuoteController } from './quote.controller';
import { QuoteRepository } from './quote.repository';
import { QuoteService } from './quote.service';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([QuoteRepository]),
  ],
  providers: [
    QuoteService,
    QuoteClient,
  ],
  controllers: [QuoteController],
  exports: [
    TypeOrmModule,
    QuoteService,
  ],
})
export class QuoteModule { }
