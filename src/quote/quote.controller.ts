import { Controller, Get, Param } from '@nestjs/common';

import { FindQuoteDto, QuoteDto } from './dto';
import { QuoteService } from './quote.service';

@Controller('quotes')
export class QuoteController {
  constructor(private readonly quoteService: QuoteService) { }

  @Get(':date')
  async findQuote(
    @Param() { date }: FindQuoteDto,
  ): Promise<QuoteDto> {
    const quote = await this.quoteService.findQuote(date);
    return new QuoteDto(quote);
  }

}
