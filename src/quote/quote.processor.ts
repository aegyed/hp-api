import { Process, Processor } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { add, formatISO } from 'date-fns';

import { QueueName } from '../core/queue/queue.module';
import { QuoteService } from './quote.service';

@Processor(QueueName.Quote)
export class QuoteProcessor {
  private readonly logger = new Logger(QuoteProcessor.name);

  constructor(
    private readonly quoteService: QuoteService,
  ) { }

  @Process('import-quote')
  async handleImportQuote(job: Job<unknown>): Promise<void> {
    this.logger.debug('Importing quotes ...');
    const tomorrow = add(new Date(), { days: 1 });
    const tomorrowIsoDate = formatISO(tomorrow, { representation: 'date' });

    try {
      await this.quoteService.importQuote(tomorrowIsoDate);
      await job.moveToCompleted();
    } catch (err) {
      this.logger.error('Failed to import quotes!', err);
    }
    return void (0);
  }

}