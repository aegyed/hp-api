import { Injectable, Logger } from '@nestjs/common';

import { Quote, QuoteClient } from './quote.client';
import { QuoteEntity } from './quote.entity';
import { QuoteRepository } from './quote.repository';

@Injectable()
export class QuoteService {
  private readonly logger = new Logger(QuoteService.name);

  constructor(
    private readonly quoteClient: QuoteClient,
    private readonly quoteRepository: QuoteRepository,
  ) { }

  async findQuote(date: string): Promise<QuoteEntity> {
    return await this.quoteRepository.findOneOrFail(date);
  }

  async importQuote(date: string): Promise<QuoteEntity> {
    this.logger.debug(`Importing quote for date "${date}".`)

    const res: Quote = await this.quoteClient.fetchQuote();

    const quote = await this.quoteRepository.save({
      date: res.date,
      author: res.author,
      quote: res.quote,
    });

    return quote;
  }

}
