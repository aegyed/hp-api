import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';

interface ApiResponse {
  readonly success: string;
  readonly contents: {
    quotes: Quote[]
  };
}

export interface Quote {
  readonly id: number;
  readonly quote: string;
  readonly author: string;
  readonly tags: string[];
  readonly category: string;
  readonly language: string;
  readonly title: string;
  readonly date: string;
}

/*
 * Api client for quotes api.
 * Api docs: https://quotes.rest/
 */

@Injectable()
export class QuoteClient {
  constructor(
    private readonly httpService: HttpService,
  ) { }

  private apiUrl = 'https://quotes.rest';

  async fetchQuote(): Promise<Quote> {
    const url = `${this.apiUrl}/qod?category=inspire&language=en`;

    const res: AxiosResponse<ApiResponse> = await this.httpService.get(url).toPromise();

    return res.data.contents.quotes[0];
  }

}
