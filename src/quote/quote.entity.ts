import { Column, Entity } from 'typeorm';

@Entity({ name: 'quote' })
export class QuoteEntity {

  @Column({ name: 'date', type: 'date', primary: true })
  date: string;

  @Column({ name: 'author', type: 'varchar' })
  author: string;

  @Column({ name: 'quote', type: 'text' })
  quote: string;

}
