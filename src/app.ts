import { ClassSerializerInterceptor, ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { useContainer } from 'class-validator';
import helmet from 'helmet';
import morgan from 'morgan';

import { AppModule } from './app.module';
import { ConfigModule } from './core/config/config.module';
import { ConfigService } from './core/config/config.service';
import { EntityNotFoundFilter } from './core/filters/entity-not-found.filter';
import { QueryFailedFilter } from './core/filters/query-failed.filter';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const configService = app.select(ConfigModule).get(ConfigService);
  const reflector = app.get(Reflector);

  app.enableCors()

  if (configService.isProduction) {
    app.enable('trust proxy');
  }

  app.use(
    helmet(),
    morgan('combined'),
  );

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
      validationError: {
        target: false,
      },
    }),
  );

  app.useGlobalInterceptors(
    new ClassSerializerInterceptor(reflector),
  );

  app.useGlobalFilters(
    new EntityNotFoundFilter(reflector),
    new QueryFailedFilter(reflector),
  );

  // Link DI container to class-validator
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  await app.listen(
    configService.get<number>('SERVER_PORT'),
    configService.get<string>('SERVER_HOST'),
  );
}

bootstrap();
