import { Module } from '@nestjs/common';

import { AuthModule } from './auth/auth.module';
import { CoreModule } from './core/core.module';
import { QuoteModule } from './quote/quote.module';
import { UserModule } from './user/user.module';
import { FastModule } from './fast/fast.module';

@Module({
  imports: [
    CoreModule.for('app'),
    AuthModule,
    UserModule,
    QuoteModule,
    FastModule,
  ],
})
export class AppModule { }
