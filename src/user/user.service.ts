import { Injectable } from '@nestjs/common';
import { Connection, FindOneOptions } from 'typeorm';

import { IPaginationOptions, paginate, Pagination } from '../core/pagination';
import { UpdateUserDto } from './dto';
import { UserEntity } from './user.entity';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  constructor(
    private readonly connection: Connection,
    private readonly userRepository: UserRepository,
  ) { }

  async findUser(id: number): Promise<UserEntity> {
    return this.userRepository.findOneOrFail(id);
  }

  async findUsers(
    paginationOptions: IPaginationOptions,
    findOptions?: FindOneOptions<UserEntity>,
  ): Promise<Pagination<UserEntity>> {
    return paginate(this.userRepository, paginationOptions, findOptions);
  }

  async updateUser(id: number, userDto: UpdateUserDto): Promise<void> {
    await this.userRepository.update(id, userDto);
    return void (0);
  }

  async deleteUser(id: number): Promise<void> {
    await this.userRepository.delete(id);
    return void (0);
  }
}
