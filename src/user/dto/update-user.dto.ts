import {
  IsEnum,
  IsNumber,
  IsOptional,
  IsString,
  IsAlpha,
  Min,
  IsPositive,
} from 'class-validator';
import { MeasurementSystem } from '../user.entity';

export class UpdateUserDto {

  @IsString()
  @IsAlpha()
  @IsOptional()
  readonly firstName?: string;

  @IsString()
  @IsAlpha()
  @IsOptional()
  readonly lastName?: string;

  @IsNumber()
  @Min(18)
  @IsOptional()
  readonly age?: number;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  readonly height?: number;

  @IsNumber()
  @IsPositive()
  @IsOptional()
  readonly weight?: number;

  @IsEnum(MeasurementSystem)
  @IsOptional()
  readonly measurementSystem?: MeasurementSystem;

}
