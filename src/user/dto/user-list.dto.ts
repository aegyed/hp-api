import { Pagination } from '../../core/pagination';
import { UserEntity } from '../user.entity';
import { UserDto } from './user.dto';

export class UserListDto {
  readonly items: UserDto[];
  readonly itemCount: number;
  readonly totalItems: number;
  readonly totalPages: number;
  readonly next?: string;
  readonly prev?: string;

  constructor(pagination: Pagination<UserEntity>) {
    this.items = pagination.items.map(entity => new UserDto(entity));
    this.itemCount = pagination.itemCount;
    this.totalItems = pagination.totalItems;
    this.totalPages = pagination.totalPages;
    this.next = pagination.next;
    this.prev = pagination.prev;
  }
}
