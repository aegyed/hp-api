import { MeasurementSystem, Role, UserEntity } from '../user.entity';

export class UserDto {
  readonly id: number;
  readonly email: string;
  readonly role: Role;
  readonly firstName: string;
  readonly lastName: string;
  readonly birthDate: Date;
  readonly height: number;
  readonly weight: number;
  readonly measurementSystem: MeasurementSystem;
  readonly lastLogin: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;

  constructor(userEntity: UserEntity) {
    this.id = userEntity.id;
    this.email = userEntity.email;
    this.role = userEntity.role;
    this.firstName = userEntity.firstName;
    this.lastName = userEntity.lastName;
    this.birthDate = userEntity.birthDate;
    this.height = userEntity.height;
    this.weight = userEntity.weight;
    this.measurementSystem = userEntity.measurementSystem;
    this.lastLogin = userEntity.lastLogin;
    this.createdAt = userEntity.createdAt;
    this.updatedAt = userEntity.updatedAt;
  }
}