import { Transform } from 'class-transformer';
import { IsEmail, IsOptional } from 'class-validator';
import { FindOptionsDto, OrderType } from '../../core/dto/find-options.dto';
import { UserEntity } from '../user.entity';

export class FindUsersDto extends FindOptionsDto<UserEntity> {

  @IsOptional()
  readonly firstName?: string;

  @IsOptional()
  @IsEmail()
  readonly email?: string;

  @IsOptional()
  @Transform(({ value }) => ({ createdAt: value === OrderType.ASC ? OrderType.ASC : OrderType.DESC }))
  order = {
    createdAt: OrderType.DESC,
  }

}