import { Repository } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';

import { UserEntity } from './user.entity';

@EntityRepository(UserEntity)
export class UserRepository extends Repository<UserEntity> {

  async findByEmail(email: string): Promise<UserEntity> {
    return this.findOne({ email });
  }

}
