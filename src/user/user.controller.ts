import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, Put, Query } from '@nestjs/common';

import { CurrentUser, Roles, RolesEnum } from '../auth/decorators';
import { FindUsersDto, UpdateUserDto, UserDto, UserListDto } from './dto';
import { UserEntity } from './user.entity';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Get('me')
  async findMe(@CurrentUser() currentUser: Partial<UserEntity>): Promise<UserDto> {
    const user = await this.userService.findUser(currentUser.id);
    return new UserDto(user);
  }

  @Get(':id')
  @Roles(RolesEnum.ADMIN)
  async findUser(@Param('id') id: number): Promise<UserDto> {
    const user = await this.userService.findUser(id);
    return new UserDto(user);
  }

  @Get()
  @Roles(RolesEnum.ADMIN)
  async findUsers(@Query() findUserDto: FindUsersDto): Promise<UserListDto> {
    const { limit, page, ...findOptions } = findUserDto;
    const pagination = await this.userService.findUsers({ limit, page }, findOptions);
    return new UserListDto(pagination);
  }

  @Put(':id')
  @Roles(RolesEnum.ADMIN)
  @HttpCode(HttpStatus.NO_CONTENT)
  async updateUser(
    @Param('id') id: number,
    @Body() userDto: UpdateUserDto,
  ): Promise<void> {
    await this.userService.updateUser(id, userDto);
    return void (0);
  }

  @Delete(':id')
  @Roles(RolesEnum.ADMIN)
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteUser(@Param('id') id: number): Promise<void> {
    await this.userService.deleteUser(id);
    return void (0);
  }

}
