import { Injectable } from '@nestjs/common';
import { ValidationArguments, ValidatorConstraint } from 'class-validator';
import { parseISO, differenceInYears } from 'date-fns';

@ValidatorConstraint({ name: 'birthDate' })
@Injectable()
export class BirthDateValidator {

  public validate(date: string): boolean {
    const diff = differenceInYears(new Date(), parseISO(date));
    console.log('diff: ', diff)
    return diff >= 18;
  }

  public defaultMessage(_args: ValidationArguments): string {
    return 'Must be at least 18 years old.';
  }

}
