import { Injectable } from '@nestjs/common';
import { ValidationArguments, ValidatorConstraint } from 'class-validator';

import { UserRepository } from '../user.repository';

@ValidatorConstraint({ name: 'isUniqueEmail', async: true })
@Injectable()
export class IsUniqueEmail {
  constructor(private readonly userRepository: UserRepository) { }

  public async validate(email: string): Promise<boolean> {
    const user = await this.userRepository.findByEmail(email);

    return user === undefined;
  }

  public defaultMessage(_args: ValidationArguments): string {
    return 'User with this email already exists.';
  }
}
