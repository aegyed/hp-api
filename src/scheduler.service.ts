import { InjectQueue } from '@nestjs/bull';
import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Queue } from 'bull';

import { QueueName } from './core/queue/queue.module';

@Injectable()
export class SchedulerService {
  private readonly logger = new Logger(SchedulerService.name);

  constructor(
    @InjectQueue(QueueName.Quote) private readonly quoteQueue: Queue,
  ) { }

  @Cron(CronExpression.EVERY_DAY_AT_MIDNIGHT)
  handleImportQuote(): void {
    this.quoteQueue.add('import-quote');
  }

}