import { Module } from '@nestjs/common';

import { CoreModule } from './core/core.module';
import { FastModule } from './fast/fast.module';
import { FastProcessor } from './fast/fast.processor';
import { QuoteModule } from './quote/quote.module';
import { QuoteProcessor } from './quote/quote.processor'

@Module({
  imports: [
    CoreModule.for('worker'),
    QuoteModule,
    FastModule,
  ],
  providers: [
    QuoteProcessor,
    FastProcessor,
  ],
})
export class WorkerModule { }
