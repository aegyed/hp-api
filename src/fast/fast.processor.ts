import { Processor, Process } from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';

import { QueueName } from '../core/queue/queue.module';
import { FastService } from './fast.service';

export interface IFast {
  fastId: number;
}

@Processor(QueueName.Fast)
export class FastProcessor {
  logger = new Logger(FastProcessor.name);

  constructor(
    private readonly fastService: FastService,
  ) { }

  @Process('goal-reached')
  async handleGoalReached(job: Job<IFast>): Promise<void> {
    this.logger.debug(`Processing fast ${job.data.fastId}`);

    try {
      await this.fastService.processFast(job.data.fastId)
      await job.moveToCompleted();
    } catch (err) {
      this.logger.error(`Failed to process fast ${job.data.fastId}!`, err);
    }
    return void (0);
  }

}