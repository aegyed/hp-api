import { FastEntity, FastType, Status } from '../fast.entity';

export class FastDto {
  readonly id: number;
  readonly fastType: FastType;
  readonly userId: number;
  readonly status: Status;
  readonly startedAt: Date;
  readonly finishedAt: Date;

  constructor(fast: FastEntity) {
    this.id = fast.id;
    this.fastType = fast.fastType;
    this.userId = fast.userId;
    this.status = fast.status;
    this.startedAt = fast.startedAt;
    this.finishedAt = fast.finishedAt;
  }
}
