import { IsDefined, IsEnum } from 'class-validator';
import { FastType } from '../fast.entity';

export class CreateFastDto {

  @IsEnum(FastType)
  @IsDefined()
  readonly fastType: FastType;

}
