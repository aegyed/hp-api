import { IsDateString, IsDefined, IsEnum, IsNotEmpty, ValidateIf } from 'class-validator';
import { FastType } from '../fast.entity';

export class UpdateFastDto {

  @ValidateIf(o => !o.startedAt || o.fastType)
  @IsDefined()
  @IsEnum(FastType)
  readonly fastType?: FastType;

  @ValidateIf(o => !o.fastType || o.startedAt)
  @IsNotEmpty()
  @IsDateString()
  readonly startedAt?: string;

}
