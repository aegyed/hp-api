import { Injectable, Logger, BadRequestException } from '@nestjs/common';
import { InjectQueue } from '@nestjs/bull';
import { Queue } from 'bull';
import { add, differenceInMilliseconds, parseISO, isAfter } from 'date-fns';

import { QueueName } from '../core/queue/queue.module';
import { FastRepository } from './fast.repository';
import { FastEntity, Status } from './fast.entity';
import { CreateFastDto, UpdateFastDto } from './dto';
import { fastDurationMap } from './fast.constant';

@Injectable()
export class FastService {
  logger = new Logger(FastService.name);

  constructor(
    private readonly fastRepository: FastRepository,
    @InjectQueue(QueueName.Fast) private readonly fastQueue: Queue,
  ) { }

  async findFast(id: number): Promise<FastEntity> {
    return this.fastRepository.findOneOrFail(id);
  }

  async findOngoingFast(userId: number): Promise<FastEntity> {
    return this.fastRepository.findOneOrFail({ userId, status: Status.Ongoing });
  }

  async createFast(userId: number, fastDto: CreateFastDto): Promise<FastEntity> {
    if (await this.fastRepository.findOne({ userId, status: Status.Ongoing })) {
      throw new BadRequestException('One fast at a time.');
    }

    const fast = await this.fastRepository.save({ ...fastDto, userId });
    await this.enqueueGoalReachedTask(fast);
    return fast;
  }

  async updateOngoingFast(userId: number, fastDto: UpdateFastDto): Promise<FastEntity> {
    const fast = await this.fastRepository.findOne({ userId, status: Status.Ongoing });
    if (!fast) {
      throw new BadRequestException('Ongoing fast not found.')
    }

    const attrs = {
      ...(fastDto.fastType) && { fastType: fastDto.fastType },
      ...(fastDto.startedAt) && { startedAt: parseISO(fastDto.startedAt) },
    }
    await this.fastRepository.update(fast.id, attrs)

    const updated = { ...fast, ...attrs };
    await this.enqueueGoalReachedTask(updated);

    return updated;
  }

  async breakOngoingFast(userId: number): Promise<FastEntity> {
    const fast = await this.fastRepository.findOne({ userId, status: Status.Ongoing });
    if (!fast) {
      throw new BadRequestException('Ongoing fast not found.')
    }

    const endDate = add(fast.startedAt, { hours: fastDurationMap[fast.fastType] })
    const currentDate = new Date();

    const attrs = {
      status: Status.Finished,
      finishedAt: currentDate,
      goalReached: isAfter(currentDate, endDate)
    }
    await this.fastRepository.update(fast.id, attrs)

    return { ...fast, ...attrs };
  }

  private async enqueueGoalReachedTask(fast: FastEntity): Promise<void> {
    const endDate = add(fast.startedAt, { hours: fastDurationMap[fast.fastType] })
    const delayInMs = differenceInMilliseconds(endDate, fast.startedAt);
    await this.fastQueue.add('goal-reached', { fastId: fast.id }, { delay: delayInMs });
  }

  public async processFast(id: number): Promise<void> {
    const fast = await this.findFast(id);
    if (!fast) {
      throw new BadRequestException('Ongoing fast not found.')
    }

    if (fast.status !== Status.Ongoing) {
      this.logger.debug('noop, user stopped ongoing fast')
      return
    }

    const endDate = add(fast.startedAt, { hours: fastDurationMap[fast.fastType] })
    const currentDate = new Date();
    const goalReached = isAfter(currentDate, endDate)
    
    if (!goalReached) {
      this.logger.debug('noop, user modified ongoing fast attributes')
      return
    }

    await this.fastRepository.update(fast.id, {
      status: Status.Finished,
      finishedAt: currentDate,
      goalReached
    })

    // TODO: notify user that fast is complete
  }

}