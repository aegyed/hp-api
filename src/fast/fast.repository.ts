import { Repository, EntityRepository } from 'typeorm';

import { FastEntity } from './fast.entity';

@EntityRepository(FastEntity)
export class FastRepository extends Repository<FastEntity> { }