import { Entity, PrimaryGeneratedColumn, Column, Index } from 'typeorm';

export enum FastType {
  TimeRestrictedFeeding13Hours = 'TRF_13',
  TimeRestrictedFeeding16Hours = 'TRF_16',
  TimeRestrictedFeeding18Hours = 'TRF_18',
  TimeRestrictedFeeding20Hours = 'TRF_20',
  TimeRestrictedFeeding36Hours = 'TRF_36',
}

export enum Status {
  Ongoing = 'ongoing',
  Finished = 'finished',
}

@Entity({ name: 'fast' })
export class FastEntity {

  @PrimaryGeneratedColumn({ name: 'id' })
  id: number;

  @Index()
  @Column({ name: 'user_id', type: 'int', nullable: false, comment: 'Refers to hp.user.id' })
  userId: number;

  @Column({ name: 'fast_type', type: 'enum', enum: FastType, nullable: false })
  fastType: FastType;

  @Index()
  @Column({ name: 'status', type: 'enum', enum: Status, nullable: false, default: Status.Ongoing })
  status: Status;

  @Column({ name: 'started_at', type: 'timestamp', precision: null, nullable: false, default: () => 'CURRENT_TIMESTAMP' })
  startedAt: Date;

  @Column({ name: 'finished_at', type: 'timestamp', precision: null, nullable: true, default: null })
  finishedAt: Date;

  @Column({ name: 'goal_reached', type: 'tinyint', width: 1, default: 0 })
  goalReached: boolean;

}