import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FastController } from './fast.controller';
import { FastService } from './fast.service';
import { FastRepository } from './fast.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([FastRepository]),
  ],
  controllers: [FastController],
  providers: [FastService],
  exports: [TypeOrmModule, FastService]
})
export class FastModule {

}