import { FastType } from './fast.entity'

export const fastDurationMap = {
  [FastType.TimeRestrictedFeeding13Hours]: 13,
  [FastType.TimeRestrictedFeeding16Hours]: 16,
  [FastType.TimeRestrictedFeeding18Hours]: 18,
  [FastType.TimeRestrictedFeeding20Hours]: 20,
  [FastType.TimeRestrictedFeeding36Hours]: 36,
}
