import { Controller, Get, Post, Body, Put } from '@nestjs/common';

import { FastService } from './fast.service';
import { FastDto, CreateFastDto, UpdateFastDto } from './dto';
import { CurrentUser } from '../auth/decorators';
import { UserEntity } from 'src/user/user.entity';
import { FastEntity } from './fast.entity';

@Controller('fasts')
export class FastController {
  constructor(private readonly fastService: FastService) { }

  @Post()
  async createFast(
    @CurrentUser() user: UserEntity,
    @Body() fastDto: CreateFastDto,
  ): Promise<FastDto> {
    const fast = await this.fastService.createFast(user.id, fastDto);
    return new FastDto(fast);
  }

  @Get('/ongoing')
  async findOngoingFast(
    @CurrentUser() user: UserEntity,
  ): Promise<FastDto> {
    const fast = await this.fastService.findOngoingFast(user.id);
    return new FastDto(fast);
  }

  @Put('/ongoing')
  async updateOngoingFast(
    @CurrentUser() user: UserEntity,
    @Body() fastDto: UpdateFastDto,
  ): Promise<FastEntity> {
    const fast = await this.fastService.updateOngoingFast(user.id, fastDto);
    return fast;
  }

  @Put('/ongoing/break')
  async breakOngoingFast(@CurrentUser() { id: userId }: UserEntity): Promise<FastEntity> {
    const fast = await this.fastService.breakOngoingFast(userId);
    return fast;
  }

}
